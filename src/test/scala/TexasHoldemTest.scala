import engine.{Dealer, ValidationException}
import model.card.Ranks
import org.scalatest.funsuite.AnyFunSuite

class TexasHoldemTest extends AnyFunSuite {

  val givenCombinations: String = "4cKs4h8s7s Ad4s Ac4d As9s KhKd 5d6d"
  val requiredResult: String = "Ac4d=Ad4s 5d6d As9s KhKd"

  val secondCombinations: String = "2h3h4h5d8d KdKs 9hJh"
  val secondRequiredResult: String = "KdKs 9hJh"

  val emptyTexasCombinations: String = ""
  val invalidTexasCombinations: String = "4cZs0X8s7s Ad4s Ac4d As9s KhKd 5d6d"
  val lackOfPlayersCombinations: String = "4cKs4h8s7s Ad4s"
  val wrongBoardSizeCombinations: String = "4cKs4h8s Ad4s Ac4d As9s KhKd 5d6d"

  val thirdCombinations: String = "9h9sAdAsTd 7c7h Qd5s KsKc ThAh 5h3c 3s3d QhTc Qc8s 6c4d"
  val thirdResult: String = "3s3d=5h3c=6c4d=7c7h Qc8s=Qd5s QhTc KsKc ThAh"

  val fourthCombinations: String = "4c8c8dTcTd JsTh 6c5s 4d3d 6d8s 2h3s TsQs Kc9s Qc7h 6hKs"
  val fourthResult: String = "2h3s=4d3d 6c5s Qc7h 6hKs=Kc9s 6d8s JsTh=TsQs"

  test("game logic is right") {
    val result = Dealer
      .startGame("texas")
      .apply(givenCombinations)
      .play()
      .resultAsCollectionOfTuples

    assert(result.head._1.handCards.head.rank.equals(Ranks.ACE))
    assert(result(1)._1.handCards.head.rank.equals(Ranks.ACE))
    assert(result(2)._1.handCards.head.rank.equals(Ranks.FIVE))
    assert(result(3)._1.handCards.head.rank.equals(Ranks.ACE))
    assert(result(4)._1.handCards.head.rank.equals(Ranks.KING))
  }

  test("main flow with first conditions") {
    val result = Dealer
      .startGame("texas")
      .apply(givenCombinations)
      .play().resultAsStringLine

    assert(result.equals(requiredResult))
  }

  test("main flow with second conditions") {
    val result = Dealer
      .startGame("texas")
      .apply(secondCombinations)
      .play().resultAsStringLine

    assert(result.equals(secondRequiredResult))
  }

  test("exception in case of wrong game type") {
    assertThrows[ValidationException](Dealer
      .startGame("unknown")
      .apply(givenCombinations)
      .play())
  }

  test("exception in case of empty game string") {
    assertThrows[ValidationException](Dealer
      .startGame("texas")
      .apply(emptyTexasCombinations)
      .play())
  }

  test("exception in case lack of players") {
    assertThrows[ValidationException](Dealer
      .startGame("texas")
      .apply(lackOfPlayersCombinations)
      .play())
  }

  test("exception in case of wrong board cards count") {
    assertThrows[ValidationException](Dealer
      .startGame("texas")
      .apply(wrongBoardSizeCombinations)
      .play())
  }

  test("three data case") {
    val result = Dealer
      .startGame("texas")
      .apply(thirdCombinations)
      .play().resultAsStringLine
    assert(result.equals(thirdResult))
  }

  test("four data case") {
    val result = Dealer
      .startGame("texas")
      .apply(fourthCombinations)
      .play().resultAsStringLine
    assert(result.equals(fourthResult))
  }
}