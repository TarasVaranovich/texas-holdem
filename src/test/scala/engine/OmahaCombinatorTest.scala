package engine

import model.Player
import model.card.{Card, Ranks, Suits}
import org.scalatest.funsuite.AnyFunSuite

class OmahaCombinatorTest extends AnyFunSuite {

  val FOUR_BY_TWO_COMBINATIONS: Int = 6
  val FIVE_BY_THREE_COMBINATIONS: Int = 10

  val players = List(
    Player(List(
      Card(Ranks.TWO, Suits.SPADES),
      Card(Ranks.KING, Suits.HEARTS),
      Card(Ranks.FIVE, Suits.CLUB),
      Card(Ranks.SIX, Suits.CLUB)
    )),
    Player(List(
      Card(Ranks.ACE, Suits.SPADES),
      Card(Ranks.NINE, Suits.DIAMOND),
      Card(Ranks.KING, Suits.CLUB),
      Card(Ranks.TEN, Suits.DIAMOND)
    )),
    Player(List(
      Card(Ranks.SEVEN, Suits.HEARTS),
      Card(Ranks.EIGHT, Suits.CLUB),
      Card(Ranks.KING, Suits.SPADES),
      Card(Ranks.JACK, Suits.CLUB)
    )),
    Player(List(
      Card(Ranks.THREE, Suits.DIAMOND),
      Card(Ranks.FOUR, Suits.DIAMOND),
      Card(Ranks.ACE, Suits.HEARTS),
      Card(Ranks.QUEEN, Suits.CLUB)
    ))
  )

  val boardCards = List(
    Card(Ranks.ACE, Suits.DIAMOND),
    Card(Ranks.TWO, Suits.DIAMOND),
    Card(Ranks.THREE, Suits.SPADES),
    Card(Ranks.JACK, Suits.DIAMOND),
    Card(Ranks.QUEEN, Suits.DIAMOND))

  test("all possible provided by players combinations") {
    val result =
      OmahaCombinator.allPlayersCombinations(players).values.toList

    assert(result.forall(_.toList.size == FOUR_BY_TWO_COMBINATIONS))
  }

  test("all possible provided by board cards combinations") {
    val result = OmahaCombinator.allBoardCards(boardCards)

    assert(result.size == FIVE_BY_THREE_COMBINATIONS)
  }

  test("all players hand value candidates generation") {
    val result = OmahaCombinator.allPlayerHandValueCandidates(players, boardCards)

    assert(result.forall(_._2.size == (FOUR_BY_TWO_COMBINATIONS * FIVE_BY_THREE_COMBINATIONS)))
  }
}
