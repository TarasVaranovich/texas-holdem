package engine

import model.card.{Card, Ranks, Suits}
import model.hand_value.HighCard
import org.scalatest.funsuite.AnyFunSuite

class CombinatorTest extends AnyFunSuite {

  test("parse 'high card' hand value") {
    val highCard = List(
      Card(Ranks.KING, Suits.HEARTS),
      Card(Ranks.JACK, Suits.CLUB),
      Card(Ranks.ACE, Suits.DIAMOND),
      Card(Ranks.TWO, Suits.SPADES),
      Card(Ranks.SIX, Suits.HEARTS),
      Card(Ranks.FOUR, Suits.CLUB),
      Card(Ranks.EIGHT, Suits.DIAMOND))

    assert(Combinator.highCard(highCard).isInstanceOf[HighCard])
  }

  test("parse 'pair' hand value") {
    val pair = List(
      Card(Ranks.KING, Suits.HEARTS),
      Card(Ranks.KING, Suits.CLUB),
      Card(Ranks.ACE, Suits.DIAMOND),
      Card(Ranks.EIGHT, Suits.SPADES),
      Card(Ranks.SIX, Suits.HEARTS),
      Card(Ranks.NINE, Suits.CLUB),
      Card(Ranks.FOUR, Suits.DIAMOND))

    assert(Combinator.pair(pair).isDefined)
  }

  test("parse 'two pairs' hand value") {
    val twoPairs = List(
      Card(Ranks.TWO, Suits.HEARTS),
      Card(Ranks.TWO, Suits.CLUB),
      Card(Ranks.SIX, Suits.DIAMOND),
      Card(Ranks.KING, Suits.SPADES),
      Card(Ranks.KING, Suits.HEARTS),
      Card(Ranks.FOUR, Suits.CLUB),
      Card(Ranks.NINE, Suits.DIAMOND))

    assert(Combinator.twoPairs(twoPairs).isDefined)
  }

  test("parse 'three of kind' hand value") {
    val threeOfKind = List(
      Card(Ranks.KING, Suits.HEARTS),
      Card(Ranks.KING, Suits.CLUB),
      Card(Ranks.SIX, Suits.DIAMOND),
      Card(Ranks.KING, Suits.SPADES),
      Card(Ranks.EIGHT, Suits.HEARTS),
      Card(Ranks.FOUR, Suits.CLUB),
      Card(Ranks.NINE, Suits.DIAMOND))

    assert(Combinator.threeOfKind(threeOfKind).isDefined)
  }

  test("parse best 'straight' hand value of two possible") {
    val straightBestOfTwo = List(
      Card(Ranks.EIGHT, Suits.HEARTS),
      Card(Ranks.JACK, Suits.CLUB),
      Card(Ranks.KING, Suits.DIAMOND),
      Card(Ranks.QUEEN, Suits.SPADES),
      Card(Ranks.TEN, Suits.HEARTS),
      Card(Ranks.NINE, Suits.CLUB),
      Card(Ranks.ACE, Suits.DIAMOND))

    assume(Combinator.straight(straightBestOfTwo).isDefined)
    assert(Combinator.straight(straightBestOfTwo).get.straight.max.rank.equals(Ranks.ACE))
  }

  test("parse 'straight' hand value in case of weak ace usage") {
    val straightWeakAce = List(
      Card(Ranks.FOUR, Suits.HEARTS),
      Card(Ranks.JACK, Suits.CLUB),
      Card(Ranks.TWO, Suits.DIAMOND),
      Card(Ranks.QUEEN, Suits.SPADES),
      Card(Ranks.THREE, Suits.HEARTS),
      Card(Ranks.FIVE, Suits.CLUB),
      Card(Ranks.ACE, Suits.DIAMOND))

    assert(Combinator.straight(straightWeakAce).isDefined)

  }

  test("parse 'straight' hand value in case of card duplicates") {
    val straightWithDuplicates = List(
      Card(Ranks.FOUR, Suits.HEARTS),
      Card(Ranks.THREE, Suits.SPADES),
      Card(Ranks.TWO, Suits.DIAMOND),
      Card(Ranks.ACE, Suits.SPADES),
      Card(Ranks.THREE, Suits.HEARTS),
      Card(Ranks.FIVE, Suits.CLUB),
      Card(Ranks.ACE, Suits.DIAMOND))

    assert(Combinator.straight(straightWithDuplicates).isDefined)
  }

  test("parse 'flush' hand value") {
    val flush = List(
      Card(Ranks.FOUR, Suits.HEARTS),
      Card(Ranks.QUEEN, Suits.HEARTS),
      Card(Ranks.TWO, Suits.HEARTS),
      Card(Ranks.ACE, Suits.SPADES),
      Card(Ranks.THREE, Suits.HEARTS),
      Card(Ranks.ACE, Suits.HEARTS),
      Card(Ranks.KING, Suits.HEARTS))

    assert(Combinator.flush(flush).isDefined)
  }

  test("parse 'full house' hand value") {
    val fullHouse = List(
      Card(Ranks.JACK, Suits.HEARTS),
      Card(Ranks.QUEEN, Suits.HEARTS),
      Card(Ranks.JACK, Suits.SPADES),
      Card(Ranks.QUEEN, Suits.SPADES),
      Card(Ranks.JACK, Suits.CLUB),
      Card(Ranks.QUEEN, Suits.DIAMOND),
      Card(Ranks.TEN, Suits.CLUB))

    assert(Combinator.fullHouse(fullHouse).isDefined)
  }

  test("parse 'four of kind' hand value") {
    val fourOfKind = List(
      Card(Ranks.JACK, Suits.HEARTS),
      Card(Ranks.QUEEN, Suits.HEARTS),
      Card(Ranks.JACK, Suits.SPADES),
      Card(Ranks.ACE, Suits.SPADES),
      Card(Ranks.JACK, Suits.CLUB),
      Card(Ranks.JACK, Suits.DIAMOND),
      Card(Ranks.TEN, Suits.CLUB))

    assert(Combinator.fourOfKind(fourOfKind).isDefined)
  }

  test("parse 'straight flush' hand value") {
    val straightFlash = List(
      Card(Ranks.JACK, Suits.SPADES),
      Card(Ranks.QUEEN, Suits.SPADES),
      Card(Ranks.ACE, Suits.SPADES),
      Card(Ranks.TEN, Suits.SPADES),
      Card(Ranks.JACK, Suits.CLUB),
      Card(Ranks.NINE, Suits.DIAMOND),
      Card(Ranks.KING, Suits.SPADES))

    assert(Combinator.straightFlash(straightFlash).isDefined)
  }

  test("parse two pair combination in case of three existing pairs") {
    val twoPairsCandidate = List(
      Card(Ranks.NINE, Suits.HEARTS),
      Card(Ranks.NINE, Suits.SPADES),
      Card(Ranks.ACE, Suits.DIAMOND),
      Card(Ranks.ACE, Suits.SPADES),
      Card(Ranks.TEN, Suits.DIAMOND),
      Card(Ranks.QUEEN, Suits.HEARTS),
      Card(Ranks.TEN, Suits.SPADES))

    assert(Combinator.twoPairs(twoPairsCandidate).isDefined)
  }
}