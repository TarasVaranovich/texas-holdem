package engine

import model.card.{Card, Ranks, Suits}
import model.hand_value._
import org.scalatest.funsuite.AnyFunSuite

class TexasRulesHolderTest extends AnyFunSuite {

  test("comparing two 'high card' hand values") {
    val thisHighCard = HighCard(
      Card(Ranks.KING, Suits.HEARTS),
      Set(
        Card(Ranks.QUEEN, Suits.CLUB),
        Card(Ranks.JACK, Suits.DIAMOND),
        Card(Ranks.ACE, Suits.SPADES),
        Card(Ranks.SIX, Suits.HEARTS))
    )

    val thatHighCard = HighCard(
      Card(Ranks.KING, Suits.HEARTS),
      Set(
        Card(Ranks.SIX, Suits.CLUB),
        Card(Ranks.JACK, Suits.DIAMOND),
        Card(Ranks.TEN, Suits.SPADES),
        Card(Ranks.QUEEN, Suits.HEARTS))
    )

    assert(RulesHolder.compareHighCard(thisHighCard, thatHighCard) == 1)
  }

  test("comparing two 'pair' hand values") {
    val thisPair = Pair(
      Set(Card(Ranks.QUEEN, Suits.HEARTS), Card(Ranks.QUEEN, Suits.CLUB)),
      Set(Card(Ranks.JACK, Suits.DIAMOND), Card(Ranks.ACE, Suits.SPADES), Card(Ranks.SIX, Suits.HEARTS))
    )

    val thatPair = Pair(
      Set(Card(Ranks.QUEEN, Suits.HEARTS), Card(Ranks.QUEEN, Suits.CLUB)),
      Set(Card(Ranks.JACK, Suits.DIAMOND), Card(Ranks.KING, Suits.SPADES), Card(Ranks.SIX, Suits.HEARTS))
    )

    assert(RulesHolder.comparePair(thisPair, thatPair) == 1)
  }

  test("comparing two 'two pairs' hand values") {
    val thisTwoPairs = TwoPairs(
      Set(Card(Ranks.KING, Suits.HEARTS), Card(Ranks.KING, Suits.CLUB)),
      Set(Card(Ranks.JACK, Suits.DIAMOND), Card(Ranks.JACK, Suits.SPADES)),
      Card(Ranks.SEVEN, Suits.HEARTS)
    )

    val thatTwoPairs = TwoPairs(
      Set(Card(Ranks.KING, Suits.HEARTS), Card(Ranks.KING, Suits.CLUB)),
      Set(Card(Ranks.JACK, Suits.DIAMOND), Card(Ranks.JACK, Suits.SPADES)),
      Card(Ranks.SIX, Suits.HEARTS)
    )

    assert(RulesHolder.compareTwoPairs(thisTwoPairs, thatTwoPairs) == 1)
  }

  test("comparing two 'three of kind' hand values") {
    val thisThreeOfKind = ThreeOfKind(
      Set(Card(Ranks.ACE, Suits.HEARTS), Card(Ranks.ACE, Suits.CLUB), Card(Ranks.ACE, Suits.DIAMOND)),
      Set(Card(Ranks.KING, Suits.SPADES), Card(Ranks.SIX, Suits.HEARTS))
    )

    val thatThreeOfKind = ThreeOfKind(
      Set(Card(Ranks.QUEEN, Suits.HEARTS), Card(Ranks.QUEEN, Suits.CLUB), Card(Ranks.QUEEN, Suits.DIAMOND)),
      Set(Card(Ranks.KING, Suits.SPADES), Card(Ranks.SIX, Suits.HEARTS))
    )

    assert(RulesHolder.compareThreeOfKind(thisThreeOfKind, thatThreeOfKind) == 1)
  }

  test("comparing two 'straight' hand values") {
    val thisStraight = Straight(
      Set(Card(Ranks.ACE, Suits.HEARTS), Card(Ranks.KING, Suits.SPADES), Card(Ranks.QUEEN, Suits.HEARTS),
        Card(Ranks.JACK, Suits.DIAMOND), Card(Ranks.TEN, Suits.HEARTS))
    )

    val thatStraight = Straight(
      Set(Card(Ranks.ACE, Suits.HEARTS), Card(Ranks.TWO, Suits.DIAMOND), Card(Ranks.THREE, Suits.HEARTS),
        Card(Ranks.FOUR, Suits.CLUB), Card(Ranks.FIVE, Suits.HEARTS))
    )

    assert(RulesHolder.compareStraight(thisStraight, thatStraight) == 1)
  }

  test("comparing two 'flush' hand values") {
    val thisFlush = Flush(
      Set(Card(Ranks.KING, Suits.HEARTS), Card(Ranks.FIVE, Suits.HEARTS), Card(Ranks.TWO, Suits.HEARTS),
        Card(Ranks.QUEEN, Suits.HEARTS), Card(Ranks.EIGHT, Suits.HEARTS))
    )

    val thatFlush = Flush(
      Set(Card(Ranks.KING, Suits.HEARTS), Card(Ranks.FIVE, Suits.HEARTS), Card(Ranks.THREE, Suits.HEARTS),
        Card(Ranks.QUEEN, Suits.HEARTS), Card(Ranks.EIGHT, Suits.HEARTS))
    )

    assert(RulesHolder.compareFlush(thisFlush, thatFlush) == -1)
  }

  test("comparing two 'full house' hand values") {
    val thisFullHouse = FullHouse(
      Set(Card(Ranks.KING, Suits.HEARTS), Card(Ranks.KING, Suits.CLUB), Card(Ranks.KING, Suits.SPADES)),
      Set(Card(Ranks.JACK, Suits.DIAMOND), Card(Ranks.JACK, Suits.SPADES))
    )

    val thatFullHouse = FullHouse(
      Set(Card(Ranks.KING, Suits.HEARTS), Card(Ranks.KING, Suits.CLUB), Card(Ranks.KING, Suits.SPADES)),
      Set(Card(Ranks.QUEEN, Suits.DIAMOND), Card(Ranks.QUEEN, Suits.SPADES))
    )

    assert(RulesHolder.compareFullHouse(thisFullHouse, thatFullHouse) == -1)
  }

  test("comparing two 'four of kind' hand values") {
    val thisFourOfKind = FourOfKind(
      Set(Card(Ranks.KING, Suits.HEARTS), Card(Ranks.KING, Suits.CLUB),
        Card(Ranks.KING, Suits.SPADES), Card(Ranks.KING, Suits.DIAMOND)),
      Card(Ranks.JACK, Suits.SPADES)
    )

    val thatFourOfKind = FourOfKind(
      Set(Card(Ranks.ACE, Suits.HEARTS), Card(Ranks.ACE, Suits.CLUB),
        Card(Ranks.ACE, Suits.SPADES), Card(Ranks.ACE, Suits.DIAMOND)),
      Card(Ranks.QUEEN, Suits.SPADES)
    )

    assert(RulesHolder.compareFourOfKind(thisFourOfKind, thatFourOfKind) == -1)
  }

  test("comparing two 'straight flush' hand values") {
    val thisStraightFlush = StraightFlush(
      Set(Card(Ranks.ACE, Suits.HEARTS), Card(Ranks.TWO, Suits.HEARTS), Card(Ranks.THREE, Suits.HEARTS),
        Card(Ranks.FOUR, Suits.HEARTS), Card(Ranks.FIVE, Suits.HEARTS))
    )

    val thatStraightFlush = StraightFlush(
      Set(Card(Ranks.ACE, Suits.HEARTS), Card(Ranks.KING, Suits.HEARTS), Card(Ranks.QUEEN, Suits.HEARTS),
        Card(Ranks.JACK, Suits.HEARTS), Card(Ranks.TEN, Suits.HEARTS))
    )

    assert(RulesHolder.compareStraightFlush(thisStraightFlush, thatStraightFlush) == -1)
  }
}