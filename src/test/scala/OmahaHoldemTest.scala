import engine.{Dealer, ValidationException}
import model.card.Ranks
import org.scalatest.funsuite.AnyFunSuite

class OmahaHoldemTest extends AnyFunSuite {

  val givenCombinations: String = "2s5cTh7d8c As4s5hKc Ah3hTsTc 7c9cJsQs 4h6hKsKd Ad3d6d9h"
  val requiredResult: String = "As4s5hKc Ah3hTsTc 4h6hKsKd Ad3d6d9h 7c9cJsQs"

  val wrongBoardSizeCombinations: String = "4cKs4h8s7s Ad4s Ac4d As9s KhKd 5d6d"

  test("game logic is right") {
    val result = Dealer
      .startGame("omaha")
      .apply(givenCombinations)
      .play()
      .resultAsCollectionOfTuples

    assert(result.head._1.handCards.last.rank.equals(Ranks.KING))
    assert(result(1)._1.handCards.last.rank.equals(Ranks.TEN))
    assert(result(2)._1.handCards.last.rank.equals(Ranks.KING))
    assert(result(3)._1.handCards.last.rank.equals(Ranks.NINE))
    assert(result(4)._1.handCards.last.rank.equals(Ranks.QUEEN))
  }

  test("main flow") {
    val result = Dealer
      .startGame("omaha")
      .apply(givenCombinations)
      .play().resultAsStringLine

    assert(result.equals(requiredResult))
  }

  test("exception in case of wrong board cards count") {
    assertThrows[ValidationException](Dealer
      .startGame("omaha")
      .apply(wrongBoardSizeCombinations)
      .play())
  }
}