package model

import engine.Rules
import model.card.Card
import model.hand_value.HandValue

case class GameSet(players: List[Player], boardCards: List[Card], rules: Rules) {

  def play(): GameResult = {

    val results: List[(Player, HandValue)] = rules.processGame(players, boardCards)

    GameResult(results, rules.resultResolver, rules.cardsCount)
  }
}