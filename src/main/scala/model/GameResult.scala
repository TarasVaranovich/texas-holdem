package model

import model.card.Card
import model.hand_value.HandValue

case class GameResult(result: collection.immutable.List[(Player, HandValue)],
                      ordering: Ordering[HandValue], cardsCount: Int) {

  def resultAsCollectionOfTuples: collection.immutable.List[(Player, HandValue)] = {
    result.sortBy(_._2)(ordering)
  }

  def resultAsStringLine: String = {
    resultAsCollectionOfTuples
      .scan(null)((previous, next) => {
        if (previous == null) next else if (ordering.compare(previous._2, next._2) == 0) {
          (Player(previous._1.handCards ::: next._1.handCards), previous._2)
        } else next
      }).tail.reverse.distinctBy(_._2).reverse.map(_._1).map(player => {
      if (player.handCards.size > cardsCount) {
        player.handCards
          .sliding(cardsCount).zipWithIndex.filter(entry => entry._2 % 2 == 0).map(_._1)
          .map(cards => reduceCards(cards)).toList.sorted.reduce(_ + "=" + _)
      } else reduceCards(player.handCards)
    }).reduce(_ + " " + _)
  }

  private def reduceCards(cards: List[Card]): String = {
    cards.map(card => card.rank.toAlias.toString + card.suit.toAlias.toString)
      .reduce(_ + _)
  }
}
