package model.card

case class Card(rank: Ranks.Value, suit: Suits.Value) extends Ordered[Card] {

  override def compare(that: Card): Int = {
    val thisRank: Int = this.rank.intValue(None)
    val thatRank: Int = that.rank.intValue(None)
    thisRank.compareTo(thatRank)
  }
}

object Card {

  def fromChars(rank: Char, suit: Char): Card = {
    Card(Ranks.fromAlias(rank), Suits.fromAlias(suit))
  }
}