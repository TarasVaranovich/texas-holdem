package model.card

object Ranks extends Enumeration {
  val TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE = Value

  def fromAlias(alias: Char): Ranks.Value = {
    alias match {
      case '2' => Ranks.TWO
      case '3' => Ranks.THREE
      case '4' => Ranks.FOUR
      case '5' => Ranks.FIVE
      case '6' => Ranks.SIX
      case '7' => Ranks.SEVEN
      case '8' => Ranks.EIGHT
      case '9' => Ranks.NINE
      case 'T' => Ranks.TEN
      case 'J' => Ranks.JACK
      case 'Q' => Ranks.QUEEN
      case 'K' => Ranks.KING
      case 'A' => Ranks.ACE
    }
  }

  implicit class RanksValue(rank: Value) {

    def intValue(aceValue: Option[Int]): Int = {
      rank match {
        case TWO => 2
        case THREE => 3
        case FOUR => 4
        case FIVE => 5
        case SIX => 6
        case SEVEN => 7
        case EIGHT => 8
        case NINE => 9
        case TEN => 10
        case JACK => 11
        case QUEEN => 12
        case KING => 13
        case ACE => aceValue.getOrElse(14)
      }
    }

    def toAlias: Char = {
      rank match {
        case TWO => '2'
        case THREE => '3'
        case FOUR => '4'
        case FIVE => '5'
        case SIX => '6'
        case SEVEN => '7'
        case EIGHT => '8'
        case NINE => '9'
        case TEN => 'T'
        case JACK => 'J'
        case QUEEN => 'Q'
        case KING => 'K'
        case ACE => 'A'
      }
    }
  }

}
