package model.card

object Suits extends Enumeration {
  val CLUB, DIAMOND, HEARTS, SPADES = Value

  def fromAlias(alias: Char): Suits.Value = {
    alias match {
      case 'c' => Suits.CLUB
      case 'd' => Suits.DIAMOND
      case 'h' => Suits.HEARTS
      case 's' => Suits.SPADES
    }
  }

  implicit class SuitsValue(suit: Value) {

    def toAlias: Char = {
      suit match {
        case CLUB => 'c'
        case DIAMOND => 'd'
        case HEARTS => 'h'
        case SPADES => 's'
      }
    }
  }

}
