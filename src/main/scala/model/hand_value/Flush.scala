package model.hand_value

import model.card.Card

case class Flush(flush: collection.immutable.Set[Card]) extends HandValue(6)