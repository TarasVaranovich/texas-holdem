package model.hand_value

import model.card.Card

case class TwoPairs(firstPair: collection.immutable.Set[Card],
                    secondPair: collection.immutable.Set[Card],
                    remain: Card) extends HandValue(3)
