package model.hand_value

import model.card.Card

case class Pair(pair: collection.immutable.Set[Card],
                remains: collection.immutable.Set[Card]) extends HandValue(2)
