package model.hand_value

import model.card.Card

case class ThreeOfKind(threeOfKind: collection.immutable.Set[Card],
                       remains: collection.immutable.Set[Card]) extends HandValue(4)
