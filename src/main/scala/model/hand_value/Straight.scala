package model.hand_value

import model.card.Card

case class Straight(straight: collection.immutable.Set[Card]) extends HandValue(5)