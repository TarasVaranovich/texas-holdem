package model.hand_value

import model.card.Card

case class StraightFlush(straightFlush: collection.immutable.Set[Card]) extends HandValue(9)
