package model.hand_value

import model.card.Card

case class FullHouse(threeOfKind: collection.immutable.Set[Card],
                     pair: collection.immutable.Set[Card]) extends HandValue(7)
