package model.hand_value

import model.card.Card

case class FourOfKind(fourOfKind: collection.immutable.Set[Card], remain: Card) extends HandValue(8)
