package model.hand_value

import model.card.Card

case class HighCard(highCard: Card, remains: collection.immutable.Set[Card]) extends HandValue(1)
