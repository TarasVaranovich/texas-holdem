package model

import model.card.Card

import scala.collection.immutable

case class Player(handCards: immutable.List[Card])
