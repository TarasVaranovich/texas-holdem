import engine.{Dealer, ValidationException}

import scala.io.Source

object TexasHoldem extends App {
  try {
    val holdem = if (args.nonEmpty) args(0).replace("--", "") else "texas"
    Source.fromInputStream(System.in).getLines().toList.map(line =>
      Dealer.startGame(holdem).apply(line).play().resultAsStringLine)
      .foreach(println)
  } catch {
    case ve: ValidationException => println(ve.message)
    case _: Throwable => println("Unknown exception:" + _)
  }
}