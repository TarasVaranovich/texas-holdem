package engine

import model.card.Card
import model.hand_value._

import scala.annotation.tailrec

object Combinator {
  val ACE_AFTER: Int = 14
  val ACE_BEFORE: Int = 1
  val PLAYING_CARDS: Int = 5

  //TODO: try to replace to implicit
  def buildHandValue(combination: List[Card]): HandValue = {
    straightFlash(combination)
      .getOrElse(fourOfKind(combination)
        .getOrElse(fullHouse(combination)
          .getOrElse(flush(combination)
            .getOrElse(straight(combination)
              .getOrElse(threeOfKind(combination)
                .getOrElse(twoPairs(combination)
                  .getOrElse(pair(combination)
                    .getOrElse(highCard(combination)
                    ))))))))
  }

  def straightFlash(list: List[Card]): Option[StraightFlush] = {
    val flushCandidate: List[Card] = list.groupBy(_.suit).maxBy(_._2.size)._2
    if (flushCandidate.size >= PLAYING_CARDS) {
      val straightCandidate: Option[Straight] = straight(flushCandidate)
      if (straightCandidate.nonEmpty) Option.apply(StraightFlush(straightCandidate.get.straight)) else Option.empty
    } else Option.empty
  }

  def fourOfKind(list: List[Card]): Option[FourOfKind] = {
    val fourOfKind: List[Card] = list.groupBy(_.rank).find(_._2.size == 4).map(_._2).getOrElse(List.empty)
    if (fourOfKind.nonEmpty) {
      val remain: Card = list.filterNot(_.rank.equals(fourOfKind.head.rank)).max
      Option.apply(FourOfKind(fourOfKind.toSet, remain))
    } else Option.empty
  }

  def fullHouse(list: List[Card]): Option[FullHouse] = {
    val rankGroups = list.groupBy(_.rank)
    val threeOfKind: List[Card] =
      rankGroups.filter(_._2.size >= 3).flatMap(_._2).toList.sorted.takeRight(3)
    if (threeOfKind.nonEmpty) {
      val pair: List[Card] = rankGroups
        .filterNot(_._2.head.rank.equals(threeOfKind.head.rank))
        .filter(_._2.size >= 2).flatMap(entry => entry._2).toList
      if (pair.nonEmpty) {
        Option.apply(FullHouse(threeOfKind.toSet, pair.sorted.takeRight(2).toSet))
      } else Option.empty
    } else Option.empty
  }

  def flush(list: List[Card]): Option[Flush] = {
    val flushCandidate = list.groupBy(_.suit).maxBy(_._2.size)._2
    if (flushCandidate.size >= 5)
      Option.apply(Flush(flushCandidate.sorted.takeRight(5).toSet))
    else Option.empty
  }

  def straight(list: List[Card]): Option[Straight] = {
    val aceRank: Int = if (list
      .map(card => card.rank.toAlias.toString)
      .sorted
      .reduce((previous, next) => previous + next)
      .contains("JKQT")) ACE_AFTER else ACE_BEFORE

    val straight = hasConsecutiveSeq(list, Option.apply(aceRank))
    if (straight.nonEmpty) {
      Option.apply(Straight(straight.toSet))
    } else Option.empty
  }

  @tailrec
  private def hasConsecutiveSeq(list: List[Card], aceValue: Option[Int]): List[Card] = {
    val sorted: List[Card] =
      list.distinctBy(_.rank.intValue(aceValue)).sorted(withAceValue(aceValue)).reverse

    if (sorted.size < 5) return List.empty[Card]

    val checksum: Int = sorted.take(PLAYING_CARDS)
      .map(_.rank.intValue(aceValue))
      .reduce((previous, next) => if (previous - next == 1) next else 0)
    if (checksum.equals(sorted(4).rank.intValue(aceValue))) {
      sorted.take(PLAYING_CARDS)
    } else {
      if (list.size > PLAYING_CARDS) {
        hasConsecutiveSeq(sorted.tail, aceValue)
      } else {
        List.empty[Card]
      }
    }
  }

  private def withAceValue(aceValue: Option[Int]): Ordering[Card] = new Ordering[Card] {
    override def compare(thisCard: Card, thatCard: Card): Int = {
      val thisRank: Int = thisCard.rank.intValue(aceValue)
      val thatRank: Int = thatCard.rank.intValue(aceValue)
      thisRank.compareTo(thatRank)
    }
  }

  def threeOfKind(list: List[Card]): Option[ThreeOfKind] = {
    val threeOfKind: List[Card] =
      list.groupBy(_.rank).find(_._2.size == 3).map(entry => entry._2).getOrElse(List.empty[Card])
    if (threeOfKind.nonEmpty) {
      val remains: Set[Card] = list.filterNot(card => threeOfKind.contains(card)).sorted.takeRight(2).toSet
      Option.apply(ThreeOfKind(threeOfKind.toSet, remains))
    } else Option.empty
  }

  def twoPairs(list: List[Card]): Option[TwoPairs] = {
    val pairs: List[Card] = list.groupBy(_.rank).filter(_._2.size == 2).flatMap(entry => entry._2).toList.sorted
    if (pairs.size >= 4) {
      val twoPairs = pairs.sorted.reverse.take(4)
      val remain: Card = list.filterNot(card => twoPairs.contains(card)).sorted.reverse.head
      Option.apply(TwoPairs(twoPairs.take(2).toSet, twoPairs.takeRight(2).toSet, remain))
    } else Option.empty
  }

  def pair(list: List[Card]): Option[Pair] = {
    val pair: Option[List[Card]] = list.groupBy(_.rank).find(_._2.size == 2).map(_._2)
    if (pair.nonEmpty) {
      val remains: Set[Card] = list
        .filterNot(card => pair.get.contains(card)).sorted.takeRight(3).toSet
      Option.apply(Pair(pair.get.toSet, remains))
    } else Option.empty
  }

  def highCard(list: List[Card]): HighCard = {
    val highCard: Card = list.max
    val remains: Set[Card] = list.sorted.reverse.tail.take(4).toSet
    HighCard(highCard, remains)
  }
}
