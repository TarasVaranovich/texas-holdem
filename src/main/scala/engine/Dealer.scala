package engine

import model.GameSet

object Dealer {

  def startGame(gameType: String): (String) => GameSet = {
    gameType match {
      case "texas" => Holdem.texas
      case "omaha" => Holdem.omaha
      case _ => throw ValidationException("Unknown type of game: " + gameType)
    }
  }
}