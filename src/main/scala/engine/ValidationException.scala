package engine

case class ValidationException(message: String) extends Exception
