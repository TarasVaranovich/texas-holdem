package engine

import model.card.Card
import model.{GameSet, Player}

object Holdem {

  def texas(combinations: String): GameSet = {
    buildGameSet(combinations, TexasRules)
  }

  def omaha(combinations: String): GameSet = {
    buildGameSet(combinations, OmahaRules)
  }

  private def buildGameSet(combinations: String, rules: Rules): GameSet = {
    try {
      val boardCards: collection.immutable.List[Card] = extractCards(combinations.toList.take(10))

      val players: List[Player] = combinations.split(" ").tail.toList
        .map(hand => extractCards(hand.toList)).map(handCards => Player(handCards))

      if (players.size < 2) throw ValidationException("Lack of players for game: " + combinations)

      if (players.forall(player => player.handCards.size != rules.cardsCount))
        throw ValidationException("In 'texas' holdem each player has 2 cards.")

      if (boardCards.size != 5)
        throw ValidationException("Inappropriate board cards count for game: " + combinations)

      GameSet(players, boardCards, rules)
    } catch {
      case _: Throwable => throw ValidationException("Cannot parse game set from string: " + combinations)
    }
  }

  private def extractCards(list: List[Char]): List[Card] = {
    list
      .grouped(2)
      .map(list => Card.fromChars(list.head, list.last))
      .toList
  }
}