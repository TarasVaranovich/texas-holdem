package engine

import model.Player
import model.card.Card
import model.hand_value.HandValue

case object OmahaRules extends Rules {

  override def cardsCount: Int = 4

  override def processGame(players: List[Player], boardCards: List[Card]): List[(Player, HandValue)] = {
    OmahaCombinator.allPlayerHandValueCandidates(players, boardCards)
      .transform((player, combinations) => player -> bestHandValue(combinations))
      .values.toList
  }

  private def bestHandValue(combinations: Set[Set[Card]]): HandValue = {
    combinations
      .map(cards => handValueCombinator.apply(cards.toList)).toList
      .sorted(resultResolver).last
  }
}