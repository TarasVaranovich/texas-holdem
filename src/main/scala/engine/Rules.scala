package engine

import model.Player
import model.card.Card
import model.hand_value.HandValue

trait Rules {
  def cardsCount: Int

  def handValueCombinator: (List[Card]) => HandValue = Combinator.buildHandValue

  def resultResolver: Ordering[HandValue] = RulesHolder.compareResults

  def processGame(players: List[Player], boardCards: List[Card]): List[(Player, HandValue)]
}
