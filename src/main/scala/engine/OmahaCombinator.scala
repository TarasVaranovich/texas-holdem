package engine

import model.Player
import model.card.Card

object OmahaCombinator {

  def allPlayersCombinations(players: List[Player]): Map[Player, Set[Set[Card]]] = {
    players.zipWithIndex.toMap
      .transform((player, index) => player -> player.handCards.toSet.subsets(2).toSet)
      .values.toMap
  }

  def allBoardCards(boardCards: List[Card]): Set[Set[Card]] = {
    boardCards.toSet.subsets(3).toSet
  }

  def allPlayerHandValueCandidates(players: List[Player],
                                   boardCards: List[Card]): Map[Player, Set[Set[Card]]] = {
    val playerCombinations = allPlayersCombinations(players)
    val boardCombinations = allBoardCards(boardCards)

    playerCombinations.transform((player, combinations) => player ->
      cartesianProduct(combinations, boardCombinations))
      .values.toMap
  }

  private def cartesianProduct(playerSet: Set[Set[Card]], boardSet: Set[Set[Card]]): Set[Set[Card]] = {
    playerSet.toList
      .flatMap(playerCards => boardSet.map(boardCards => (playerCards, boardCards)))
      .map(pair => (pair._1.toList ::: pair._2.toList).toSet).toSet
  }

}
