package engine

import model.card.{Card, Ranks}
import model.hand_value._

import scala.annotation.tailrec

//TODO: define object with constants and move magic numbers there
object RulesHolder {

  def compareResults: Ordering[HandValue] = new Ordering[HandValue] {
    override def compare(thisHandValue: HandValue, thatHandValue: HandValue): Int = {
      val result: Int = thisHandValue.getWeight.compareTo(thatHandValue.getWeight)
      if (result == 0) compareInternal(thisHandValue, thatHandValue) else result
    }
  }

  private def compareInternal(thisValue: HandValue, thatValue: HandValue): Int = {
    (thisValue, thatValue) match {
      case (thisHighCard: HighCard, thatHighCard: HighCard) =>
        compareHighCard(thisHighCard, thatHighCard)
      case (thisPair: Pair, thatPair: Pair) =>
        comparePair(thisPair, thatPair)
      case (thisTwoPairs: TwoPairs, thatTwoPairs: TwoPairs) =>
        compareTwoPairs(thisTwoPairs, thatTwoPairs)
      case (thisThreeOfKind: ThreeOfKind, thatThreeOfKind: ThreeOfKind) =>
        compareThreeOfKind(thisThreeOfKind, thatThreeOfKind)
      case (thisStraight: Straight, thatStraight: Straight) =>
        compareStraight(thisStraight, thatStraight)
      case (thisFlush: Flush, thatFlush: Flush) =>
        compareFlush(thisFlush, thatFlush)
      case (thisFullHouse: FullHouse, thatFullHouse: FullHouse) =>
        compareFullHouse(thisFullHouse, thatFullHouse)
      case (thisFourOfKind: FourOfKind, thatFourOfKind: FourOfKind) =>
        compareFourOfKind(thisFourOfKind, thatFourOfKind)
      case (thisStraightFlush: StraightFlush, thatStraightFlush: StraightFlush) =>
        compareStraightFlush(thisStraightFlush, thatStraightFlush)
    }
  }

  def compareHighCard(thisHighCard: HighCard, thatHighCard: HighCard): Int = {
    val result: Int = thisHighCard.highCard.rank.intValue(None).compareTo(thatHighCard.highCard.rank.intValue(None))
    if (result == 0) compareRecursive(thisHighCard.remains.toList, thatHighCard.remains.toList) else result
  }

  def comparePair(thisPair: Pair, thatPair: Pair): Int = {
    val result: Int = thisPair.pair.head.rank.intValue(None).compareTo(thatPair.pair.head.rank.intValue(None))
    if (result == 0) compareRecursive(thisPair.remains.toList, thatPair.remains.toList) else result
  }

  def compareTwoPairs(thisTwoPairs: TwoPairs, thatTwoPairs: TwoPairs): Int = {
    val thisPairs = thisTwoPairs.firstPair.toList ::: thisTwoPairs.secondPair.toList
    val thatPairs = thatTwoPairs.firstPair.toList ::: thatTwoPairs.secondPair.toList
    val maxPairValueResult = thisPairs.max.rank.intValue(None).compareTo(thatPairs.max.rank.intValue(None))
    if (maxPairValueResult == 0) {
      val minPairValueResult = thisPairs.min.rank.intValue(None).compareTo(thatPairs.min.rank.intValue(None))
      if (minPairValueResult == 0)
        thisTwoPairs.remain.rank.intValue(None).compareTo(thatTwoPairs.remain.rank.intValue(None))
      else minPairValueResult
    } else maxPairValueResult
  }

  def compareThreeOfKind(thisThreeOfKind: ThreeOfKind, thatThreeOfKind: ThreeOfKind): Int = {
    val result: Int = thisThreeOfKind.threeOfKind.head.rank.intValue(None)
      .compareTo(thatThreeOfKind.threeOfKind.head.rank.intValue(None))
    if (result == 0) compareRecursive(thisThreeOfKind.remains.toList, thatThreeOfKind.remains.toList) else result
  }

  def compareStraight(thisStraight: Straight, thatStraight: Straight): Int = {
    thisStraight.straight.max.rank.intValue(aceRank(thisStraight.straight))
      .compareTo(thatStraight.straight.max.rank.intValue(aceRank(thatStraight.straight)))
  }

  def compareFlush(thisFlush: Flush, thatFlush: Flush): Int = {
    compareRecursive(thisFlush.flush.toList, thatFlush.flush.toList)
  }

  def compareFullHouse(thisFullHouse: FullHouse, thatFullHouse: FullHouse): Int = {
    val result: Int = thisFullHouse.threeOfKind.head.rank.intValue(None)
      .compareTo(thatFullHouse.threeOfKind.head.rank.intValue(None))
    if (result == 0) thisFullHouse.pair.head.rank.intValue(None)
      .compareTo(thatFullHouse.pair.head.rank.intValue(None)) else result
  }

  def compareFourOfKind(thisFourOfKind: FourOfKind, thatFourOfKind: FourOfKind): Int = {
    val result: Int = thisFourOfKind.fourOfKind.head.rank.intValue(None)
      .compareTo(thatFourOfKind.fourOfKind.head.rank.intValue(None))
    if (result == 0) thisFourOfKind.remain.rank.intValue(None)
      .compareTo(thatFourOfKind.remain.rank.intValue(None)) else result
  }

  def compareStraightFlush(thisStraightFlush: StraightFlush, thatStraightFlush: StraightFlush): Int = {
    thisStraightFlush.straightFlush.max.rank.intValue(aceRank(thisStraightFlush.straightFlush))
      .compareTo(thatStraightFlush.straightFlush.max.rank.intValue(aceRank(thatStraightFlush.straightFlush)))
  }

  private def aceRank(straight: Set[Card]): Option[Int] = {
    val maxRank: Option[Card] = straight.find(_.rank.equals(Ranks.KING))
    val aceRank = if (maxRank.isDefined) 14 else 1
    Option.apply(aceRank)
  }

  @tailrec
  private def compareRecursive(thisRemains: List[Card], thatRemains: List[Card]): Int = {
    val thisSorted: List[Card] = thisRemains.sorted.reverse
    val thatSorted: List[Card] = thatRemains.sorted.reverse
    val result: Int = thisSorted.head.rank.intValue(None).compareTo(thatSorted.head.rank.intValue(None))
    if (result == 0 && thisSorted.size > 1 && thatSorted.size > 1) {
      compareRecursive(thisSorted.tail, thatSorted.tail)
    } else result
  }
}
