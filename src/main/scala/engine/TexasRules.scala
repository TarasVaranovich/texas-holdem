package engine

import model.Player
import model.card.Card
import model.hand_value.HandValue

case object TexasRules extends Rules {
  override def cardsCount: Int = 2

  override def processGame(players: List[Player], boardCards: List[Card]): List[(Player, HandValue)] = {
    players
      .map(player => (player, this.handValueCombinator(player.handCards ::: boardCards)))
  }
}
