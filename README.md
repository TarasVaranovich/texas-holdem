## Required libraries

1. Unix-like system. Application tested on macOS Catalina 10.15.6
2. JRE 1.8 or above. Application tested with java 14 2020-03-17
3. Installed scala build tool. Application tested with sbt 1.3.13
4. Installed scala. Application tested with scala 2.13.3

---

## Application usage
1. Navigate to application sources root directory ~/texas-holdem by terminal
2. Run command 'sbt run' for texas holdem or 'sbt "run --omaha"' for omaha
3. Input holdem combinations lines. Each line contains one game set
4. Press 'control + D' to stop input
5. See calculated results
 