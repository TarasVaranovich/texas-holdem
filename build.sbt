name := "texas-holdem"

version := "0.1"

scalaVersion := "2.13.3"

val TEST_VERSION = "3.1.0"
libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % TEST_VERSION % Test
)
